#!/bin/bash

# Find project root directory
project_root=$(git rev-parse --show-toplevel 2>/dev/null)
if [[ -z "$project_root" ]]; then
  echo "Error: Project root directory not found. Make sure you are inside a Git repository."
  exit 1
fi

# Default flag values
REBUILD=0
EXECUTE=0
CLEAN=0
TEST=0

# Process command line options
while getopts ":dgerct" opt; do
  case $opt in
    d)
      CMAKE_CXX_FLAGS_DEBUG="${CMAKE_CXX_FLAGS_DEBUG} -DSMT_DEBUG"
      REBUILD=1
      ;;
    g)
      CMAKE_CXX_FLAGS_DEVELOP="${CMAKE_CXX_FLAGS_DEVELOP} -DSMT_DEBUG"
      REBUILD=1
      ;;
    r)
      REBUILD=1
      EXECUTE=1  # Set EXECUTE flag when rebuilding
      ;;
    e)
      EXECUTE=1
      ;;
    c)
      CLEAN=1
      ;;
    t)
      TEST=1
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done

# Set project name based on flags
SMT_PROJECT_NAME="smt"
if [ -n "$CMAKE_CXX_FLAGS_DEBUG" ]; then
  SMT_PROJECT_NAME="${SMT_PROJECT_NAME}-d"
  BUILD_DIR="smt_d_build"
elif [ -n "$CMAKE_CXX_FLAGS_DEVELOP" ]; then
  SMT_PROJECT_NAME="${SMT_PROJECT_NAME}-g"
  BUILD_DIR="smt_g_build"
else
  BUILD_DIR="smt_build"
fi

# Set build directory based on flags
SMT_BUILD_DIR="$project_root/build/$BUILD_DIR"

# Clean build directory if CLEAN flag is set
if [ $CLEAN -eq 1 ]; then
  rm -rf "$SMT_BUILD_DIR"
fi

# Create build directory if it doesn't exist
mkdir -p "$SMT_BUILD_DIR"

# Change to build directory
cd "$SMT_BUILD_DIR" || exit

export CMAKE_EXPORT_COMPILE_COMMANDS=1

# Run CMake if REBUILD flag is set or if CMakeCache.txt doesn't exist
if [ $REBUILD -eq 1 ] || [ ! -f "CMakeCache.txt" ]; then
  echo cmake "$CMAKE_CXX_FLAGS_DEBUG" "$CMAKE_CXX_FLAGS_DEVELOP" "$project_root"
  cmake "$CMAKE_CXX_FLAGS_DEBUG" "$CMAKE_CXX_FLAGS_DEVELOP" "$project_root"
fi

# Build
make

ln -sf "$SMT_BUILD_DIR/compile_commands.json" "$project_root/compile_commands.json"

if [ $TEST -eq 1 ]; then
  for test in "$project_root/tests/*"; do
    # Execute tests
    $test
  done
fi

if [ $EXECUTE -eq 1 ]; then
  # Execute the program
  $project_root/exec/$SMT_PROJECT_NAME
fi

