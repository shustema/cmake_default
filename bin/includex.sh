#!/bin/bash

# Function to print a divider line
function print_divider {
  printf "\n%s\n" "----------------------------------------"
  printf  "\t%s" "$1"
  printf "\n%s\n" "----------------------------------------"
}

print_divider "Linking includex"

# Function to search for __GLOBAL_INCLUDE__ or __LOCAL_INCLUDE__ in the first 2 lines of .h files in a directory and create symbolic links
function process_directory {
  local directory=$1
  local include_type=$2
  local target_dir=$3

  # Create the target directory if it doesn't exist
  mkdir -p "$target_dir"

  # Process each .h file in the directory
  find "$directory" -type f -name "*.h" | while read -r file; do
    # Check if the include type is found in the file
    if head -n 2 "$file" | grep -q "/\* $include_type \*/"; then
      # Create the directory path inside the target includex
      local target_file_directory="$target_dir/$(basename "$directory")"
      mkdir -p "$target_file_directory"

      local target_file="$target_file_directory/$(basename "$file")"

      # Check if the link already exists and points to the correct file
      if [[ -L "$target_file" ]]; then
        local existing_link=$(readlink -f "$target_file")
        local expected_link="${project_root}/${file}"
        if [[ "$existing_link" != "$expected_link" ]]; then
          echo "Error: Link $target_file already exists but points to $existing_link instead of $expected_link"
          exit 1
        fi
      else
        # Create a symbolic link
        ln -rs "$file" "$target_file"
        #echo "Created link: $target_file -> $file"
      fi
    fi
  done
}

# Find project root directory
project_root=$(git rev-parse --show-toplevel 2>/dev/null)
if [[ -z "$project_root" ]]; then
  echo "Error: Project root directory not found. Make sure you are inside a Git repository."
  exit 1
fi

# Remove existing includex directories except for the top-level includex
find "$project_root" -type d -name "includex" -exec rm -rf {} +

# Process the top-level includex
process_directory "$project_root/include" "__GLOBAL_INCLUDE__" "$project_root/includex"
process_directory "$project_root/include" "__GLOBAL_INCLUDE__" "$project_root/src/includex"

# Process modules and their subdirectories
find "$project_root/src" -mindepth 1 -maxdepth 1 -type d -not -name "includex" | while read -r module; do
  echo "Processing module: $module"

  # Process module files for global includes
  process_directory "$module" "__GLOBAL_INCLUDE__" "$project_root/includex"
  process_directory "$module" "__GLOBAL_INCLUDE__" "$project_root/src/includex"

  # Process subdirectories within the module
  find "$module" -type d -not -name "includex" | while read -r subdirectory; do
    echo "Processing subdirectory: $subdirectory"

    subdirectory_includex="$subdirectory/includex"
    base_dir="$project_root/src"

    # Use find command to search for directories in the base directory and print their paths
    module_name=$project_root/src/${module#"$base_dir/"}

    # Process subdirectory files for global and local includes
    process_directory "$project_root/include" "__GLOBAL_INCLUDE__" "$subdirectory_includex"
    process_directory "$module_name" "__GLOBAL_INCLUDE__" "$subdirectory_includex"
    process_directory "$module_name" "__LOCAL_INCLUDE__" "$subdirectory_includex"
  done
done

print_divider "Finished linking includex"

