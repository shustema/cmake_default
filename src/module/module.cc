#include "module.h"

#include "module/inner.h"

#include <iostream>

void helloWorld() {
  do_nothing();
  std::cout << "Hello World" << std::endl;
}

// Module1 implementation here
