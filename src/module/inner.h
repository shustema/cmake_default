/* __LOCAL_INCLUDE__ */
#pragma once
#ifndef __INNER_H__
#define __INNER_H__

#include <iostream> 

inline void do_nothing() { std::cout << "nothing\n"; }

#endif // !__INNER_H__
