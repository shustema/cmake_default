#include <gtest/gtest.h>

#include "module/module.h"
#include "module/inner.h"

// Example test fixture
class MyTestSuite : public ::testing::Test {
protected:
  // Per-test-suite set-up
  static void SetUpTestSuite() {
    // Add any initialization code specific to the entire test suite
  }

  // Per-test-suite tear-down
  static void TearDownTestSuite() {
    // Add any clean-up code specific to the entire test suite
  }

  // Per-test set-up
  void SetUp() override {
    // Add any initialization code specific to each test
  }

  // Per-test tear-down
  void TearDown() override {
    // Add any clean-up code specific to each test
  }
};

std::string ImDumb() { return "I'm a big dumb dummy and I eat poopy"; }

// Example test case
TEST_F(MyTestSuite, ExampleTest) {
  // Test case code
  do_nothing();
  ASSERT_TRUE(true);
  ASSERT_TRUE(ImDumb() == "I'm a big dumb dummy and I eat poopy");
}

// Run the tests
int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();

}
