#include <iostream>

#include "include/core.h"
#include "module/module.h"

int main() {
  helloWorld();

#ifdef SMT_DEBUG
  std::cout << "Debug Mode" << std::endl;
#endif

#ifdef SMT_DEVELOP
  std::cout << "Develop Mode" << std::endl;
#endif

  return 0;
}
